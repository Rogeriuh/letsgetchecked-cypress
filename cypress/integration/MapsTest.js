describe('Google Maps Test', () => {

    let city = "Dublin";

    before(() => {
        cy.visit("/");
    })

    it('Confirm Consent Page', () => {
        cy.get("button[aria-label]")
            .click();
    })

    it('Enter Dublin in the search box and Search', () => {
        cy.get("#searchboxinput")
            .type(city);

        cy.get("#searchbox-searchbutton")
            .click();
    })

    it('Verify left panel has "Dublin" as a headline text', () => {
        cy.get("div#pane h1")
            .should("contains.text", city);
    })

    it('Click Directions icon', () => {
        cy.get("div#pane button img[src='//www.gstatic.com/images/icons/material/system/1x/directions_white_18dp.png']")
            .click({ force: true });
    })

    it('Verify destination field is "Dublin"', () => {
        cy.get("#sb_ifc52 > input")
            .invoke('attr', 'aria-label')
            .should('contains', city);
    })
})
