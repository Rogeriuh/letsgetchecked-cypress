# LetsGetChecked

Repo with QA Engineer - Technical Challenge Cypress

## Installation and Run

- Install Node.js

- Install Cypress and Dependencies (run on project folder)

```bash
npm i 
```

- Run tests
```bash
npm run cypress:open 
```

## Javascript Files with test

```bash
cypress/integration/MapsTest.js
```

## Cypress Configs

Here you can check all Gherkin

```bash
cypress.json
```



Thanks,
Rogério Castro